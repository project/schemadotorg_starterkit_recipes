<?php

declare(strict_types=1);

namespace Drupal\schemadotorg_starterkit_recipes_umami_content;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Component\Utility\Html;
use Drupal\demo_umami_content\InstallHelper;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

include \Drupal::root() . '/core/profiles/demo_umami/modules/demo_umami_content/src/InstallHelper.php';

/**
 * Defines a helper class for importing default content.
 *
 * @internal
 *   This code is only for use by the Umami demo: Content module.
 * @phpstan-ignore-next-line
 */
class SchemaDotOrgStarterkitRecipesUmamiContentInstallHelper extends InstallHelper {

  /**
   * The application's root directory.
   */
  protected string $appRoot;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->appRoot = $container->getParameter('app.root');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function importContent(): void {
    $import_users_method = DeprecationHelper::backwardsCompatibleCall(
      currentVersion: \Drupal::VERSION,
      deprecatedVersion: '10.3',
      currentCallable: fn() => 'importUsers',
      deprecatedCallable: fn() => 'importEditors',
    );
    $this->getModulePath()
      ->$import_users_method()
      ->importContentFromFile('taxonomy_term', 'recipe_category')
      ->importContentFromFile('media', 'image')
      ->importContentFromFile('node', 'recipe');
  }

  /**
   * {@inheritdoc}
   */
  protected function getModulePath() {
    $this->module_path = $this->appRoot . '/core/profiles/demo_umami/modules/demo_umami_content';
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function processRecipe(array $data, $langcode) {
    $mapping = [
      'field_media_image' => 'schema_image',
      'field_summary' => 'body',
      'field_recipe_category' => 'schema_recipe_category',
      'field_preparation_time' => 'schema_prep_time',
      'field_cooking_time' => 'schema_cook_time',
      'field_difficulty' => 'schema_edu_level',
      'field_number_of_servings' => 'schema_recipe_yield',
      'field_ingredients' => 'schema_recipe_ingredient',
      'field_recipe_instruction' => 'schema_recipe_instructions',
    ];

    $values = parent::processRecipe($data, $langcode);

    // Trim ingredients.
    foreach ($values['field_ingredients'] as &$item) {
      $item['value'] = trim($item['value']);
    }

    // Set summary to allowed format.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', 'recipe', 'body');
    $allowed_formats = $field_config->getSetting('allowed_formats');
    if ($allowed_formats) {
      $values['field_summary'][0]['format'] = reset($allowed_formats);
    }

    // Parse instructions.
    $dom = Html::load($values['field_recipe_instruction'][0]['value']);
    $items = [];
    foreach ($dom->getElementsByTagName('li') as $dom_node) {
      $items[] = ['value' => $dom_node->textContent];
    }
    $values['field_recipe_instruction'] = $items;

    return $this->applyMapping($values, $mapping);
  }

  /**
   * Apply mapping from Umami fields to Schema.org fields.
   *
   * @param array $values
   *   An associative array of values.
   * @param array $mapping
   *   An associative array containing source to destination mappings.
   *
   * @return array
   *   Mapped values.
   */
  protected function applyMapping(array $values, array $mapping): array {
    foreach ($mapping as $source => $destination) {
      if (isset($values[$source])) {
        $value = $values[$source];
        unset($values[$source]);
        $values[$destination] = $value;
      }
    }
    return $values;
  }

}
