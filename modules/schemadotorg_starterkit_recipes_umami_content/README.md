Table of contents
-----------------

* Introduction
* Features


Introduction
------------

The **Schema.org Blueprint: Recipes Starter Kit Umami Content module** 
imports the Umami demo content into the Recipe Schema.org type.


Features
--------

- Imports Umami content. 
  (/core/profiles/demo_umami/modules/demo_umami_content)
- Applies mapping from Umami fields to Schema.org fields.
