<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_starterkit_recipes\Functional;

use Drupal\Tests\schemadotorg\Functional\SchemaDotOrgBrowserTestBase;

/**
 * Tests the functionality of the Schema.org Blueprints Demo Starter Kit module.
 *
 * @group schemadotorg_starterkit_recipes
 */
class SchemaDotOrgStarterKitRecipesTest extends SchemaDotOrgBrowserTestBase {

  // phpcs:disable DrupalPractice.Objects.StrictSchemaDisabled.StrictConfigSchema
  /**
   * Disabled config schema checking temporarily until smart date fixes missing schema.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;
  // phpcs:enable DrupalPractice.Objects.StrictSchemaDisabled.StrictConfigSchema

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'schemadotorg_starterkit_recipes',
  ];

  /**
   * Test Schema.org Blueprints Demo Starter Kit: Recipe.
   */
  public function testRecipe(): void {
    $this->drupalLogin($this->rootUser);

    // Check that Schema.org mappings were created.
    // @covers schemadotorg_cms.schemadotorg_starterkit.yml
    /** @var \Drupal\schemadotorg\SchemaDotOrgMappingStorageInterface $mapping_storage */
    $mapping_storage = \Drupal::entityTypeManager()->getStorage('schemadotorg_mapping');
    $this->assertNotNull($mapping_storage->load('node.recipe'));

    // Check recipe properties/fields.
    /** @var \Drupal\schemadotorg\SchemaDotOrgMappingInterface|null $mapping */
    $mapping = $mapping_storage->load('node.recipe');
    $expected_properties = [
      'schema_cooking_method' => 'cookingMethod',
      'schema_cook_time' => 'cookTime',
      'created' => 'dateCreated',
      'changed' => 'dateModified',
      'schema_image' => 'image',
      'langcode' => 'inLanguage',
      'schema_is_family_friendly' => 'isFamilyFriendly',
      'title' => 'name',
      'schema_nutrition' => 'nutrition',
      'schema_prep_time' => 'prepTime',
      'schema_recipe_category' => 'recipeCategory',
      'schema_recipe_cuisine' => 'recipeCuisine',
      'schema_recipe_ingredient' => 'recipeIngredient',
      'schema_recipe_instructions' => 'recipeInstructions',
      'schema_recipe_yield' => 'recipeYield',
      'schema_suitable_for_diet' => 'suitableForDiet',
      'schema_total_time' => 'totalTime',
      'body' => 'text',
    ];
    $this->assertEquals($expected_properties, $mapping->getSchemaProperties());
  }

}
